<?php
/**
 * @file
 * sbdc_content_types.features.inc
 */

/**
 * Implements hook_views_api().
 */
function sbdc_content_types_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function sbdc_content_types_image_default_styles() {
  $styles = array();

  // Exported image style: circle.
  $styles['circle'] = array(
    'label' => 'Testimonial Circle',
    'effects' => array(
      2 => array(
        'name' => 'image_crop',
        'data' => array(
          'width' => 300,
          'height' => 300,
          'anchor' => 'center-center',
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: header_image_style.
  $styles['header_image_style'] = array(
    'label' => 'Header Image Styling',
    'effects' => array(
      3 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 3978,
          'height' => 1249,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function sbdc_content_types_node_info() {
  $items = array(
    'highlight_section' => array(
      'name' => t('Highlight Section'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Section Title'),
      'help' => '',
    ),
    'testimonial' => array(
      'name' => t('Featured Testimonial'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
